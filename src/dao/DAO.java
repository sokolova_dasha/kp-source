package dao;

import model.*;

import javax.ejb.Stateless;
import javax.persistence.*;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by даша on 22.04.14.
 */
@Stateless
public class DAO implements IDAOLocal {

    @PersistenceContext(unitName = "RailWayUnit")
    private EntityManager entityManagerRailWay;

    @PersistenceContext(unitName = "BookingUnit")
    private EntityManager entityManagerBooking;

    //обновление цены(измененено количество билетов)
    public void updatePrice(Price price) {
        entityManagerRailWay.merge(price);
    }
    // сохранить покупку
    public void savePurchase(Purchase purchase)  {
        entityManagerBooking.persist(purchase);
    }
   //найти рейсы по стнанциям и/или дате отправления
    public List<Trip> getRoutesAndTripBySityAndDate(String from,String to,Date day_start){
        if(day_start!=null){
        Query queryRouteAndTrip = entityManagerRailWay.createQuery("SELECT t from Trip t " +
                "WHERE t.route.station_start like :froms and t.route.station_finish like :tos and t.date_start = :custday");
        queryRouteAndTrip.setParameter("froms",from);
        queryRouteAndTrip.setParameter("tos",to);
        queryRouteAndTrip.setParameter("custday",day_start);
        return queryRouteAndTrip.getResultList();
        }
        else{
            Query queryRouteAndTrip = entityManagerRailWay.createQuery("SELECT t from Trip t " +
                    "WHERE t.route.station_start like :froms and t.route.station_finish like :tos");
            queryRouteAndTrip.setParameter("froms",from);
            queryRouteAndTrip.setParameter("tos",to);
            return queryRouteAndTrip.getResultList();
        }
    }
    // Зарегистрировать пользователя
    public void  addUser(Passenger passenger){
        entityManagerBooking.persist(passenger);
    }

    // Авторизация пользователя
    public boolean authUser(Passenger passenger){
        if (entityManagerBooking.contains(passenger)){
            return true;
        }
        else return false;
    }
    //Мои покупки
    public List<Purchase> getMyPurchase(Integer id_passenger){
        Query queryPurchase = entityManagerBooking.createQuery("SELECT p FROM Purchase p WHERE p.passenger.id = :id_passp");
        queryPurchase.setParameter("id_passp",id_passenger);
        return queryPurchase.getResultList();
    }


}
