package web;

import dao.IDAOLocal;
import model.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by даша on 28.04.14.
 */
@Stateful
@Named(value = "RailWayBean")
@SessionScoped
public class RailWayBean implements Serializable{

    public void postConstruct() {
        trip = new Trip();
        route = new Route();
        passenger = new Passenger();
        purchase = new Purchase();
    }

    // Предыдущая страница
    private String preceding_page;
    // Данные для поиска
    private Trip trip;
    private Route route;
    // Найденные рейсы
    private List<Trip> foundTrips;
    // Найденные маршруты
    private List<Route> foundRoutes;
    // Пассажир, вошедший в систему
    public Passenger passenger;
    private String password1;
    private String password2;
    // Выбранный рейс
    private Trip choseTrip;
    // Выбранная стоимость(тип)
    private Price price;
    // Указаное кол-во билетов
    private Integer count_ticket;
    // Формируемая покупка
    private Purchase purchase;
    // Покупки текущего пассажира
    private List<Purchase> myPurchases;
    private String message;

    public String getPreceding_page() {
        return preceding_page;
    }
    public void setPreceding_page(String preceding_page) {
        this.preceding_page = preceding_page;
    }
    public Trip getTrip() {
        return trip;
    }
    public void setTrip(Trip trip) {
        this.trip = trip;
    }
    public Route getRoute() {
        return route;
    }
    public void setRoute(Route route) {
        this.route = route;
    }
    public Passenger getPassenger() {
        return passenger;
    }
    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }
    public String getPassword1() {
        return password1;
    }
    public void setPassword1(String password1) {
        this.password1 = password1;
    }
    public String getPassword2() {
        return password2;
    }
    public void setPassword2(String password2) {
        this.password2 = password2;
    }
    public List<Trip> getFoundTrips() {
        return foundTrips;
    }
    public void setFoundTrips(List<Trip> foundTrips) {
        this.foundTrips = foundTrips;
    }
    public List<Route> getFoundRoutes() {
        return foundRoutes;
    }
    public void setFoundRoutes(List<Route> foundRoutes) {
        this.foundRoutes = foundRoutes;
    }
    public Trip getChoseTrip() {
        return choseTrip;
    }
    public void setChoseTrip(Trip choseTrip) {
        this.choseTrip = choseTrip;
    }
    public Purchase getPurchase() {
        return purchase;
    }
    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
    public Price getPrice() {
        return price;
    }
    public void setPrice(Price price) {
        this.price = price;
    }
    public Integer getCount_ticket() {
        return count_ticket;
    }
    public void setCount_ticket(Integer count_ticket) {
        this.count_ticket = count_ticket;
    }
    public List<Purchase> getMyPurchases() {
        return myPurchases;
    }
    public void setMyPurchases(List<Purchase> myPurchases) {
        this.myPurchases = myPurchases;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    @EJB
    IDAOLocal idaoLocal;
    //на предыдущую страницу
    public String toPreceding(){
        return preceding_page;
    }
    //на главную
    public String toIndex(){
        return "index";
    }
    //Поиск
   @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String Search(){
        setFoundTrips(idaoLocal.getRoutesAndTripBySityAndDate(route.getStation_start(), route.getStation_finish(), trip.getDate_start()));
        return "chooseTrip";
    }
    //Выбор рейса,список доступных билетов
    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public String chooseTrip(){
        purchase.setPassenger(passenger);
        purchase.setStation_start(choseTrip.getRoute().getStation_start());
        purchase.setStation_finish(choseTrip.getRoute().getStation_finish());
        purchase.setDate_start(choseTrip.getDate_start());
        purchase.setTime_start(choseTrip.getRoute().getTime_start());
        purchase.setTime_in_way(choseTrip.getRoute().getTime_in_way());
        purchase.setDate_finish(choseTrip.getDate_finish());
        return "chooseType";
    }

    // выбор типа билетов
    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public String chooseType(){
        purchase.setName_class(price.getClassTrip().getName());
        purchase.setDescription(price.getClassTrip().getDescription());
        purchase.setPrice(price.getSumma());
        return "chooseCount";
    }

    //указание количества билетов, проверка наличия билетов
    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public String formOrder(){
        //если достаточное количество билетов в наличии
        if(getCount_ticket()<=price.getCount_in_presence()){
            purchase.setSum_for_count(purchase.getPrice()*getCount_ticket());
            return "formOrder";
        }
        // если недостаточное
        else{
            setCount_ticket(null);
            return "chooseCount";
        }
    }

    //подтверждение заказа,проверка пользователя
    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public String confirmPurchases(){
        //если пользователь авторизован в системе
        if (passenger!=null){
            idaoLocal.savePurchase(purchase);
            price.setCount_in_presence(price.getCount_in_presence() - purchase.getTicket_count());
            idaoLocal.updatePrice(price);
            return "orderReady";
        }
        else{
            return "authorization";
        }

    }
    //Регистрация
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public String pageRegister(){
        return "register";
    }
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public String register(){
        if(password1.equals(password2)){
            idaoLocal.addUser(passenger);
            return "authorization";
        }
       else {
            message="Пароли не совпадают!";
            return "register";
        }
    }
    //Авторизация
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public String auth(){
       if (idaoLocal.authUser(passenger)){
           return preceding_page;
       }
        else {
           message="Логин или пароль указаны неверно!!!";
           return "authorization";
       }
    }
    //Мои заказы
    public String showMyPurchases(){
        if(passenger!=null)  {
            setMyPurchases(idaoLocal.getMyPurchase(passenger.getId()));
                return "myPurchases";
        }
        else{
            return "authorization";
        }
    }


}
