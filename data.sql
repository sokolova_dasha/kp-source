insert into db_kp6java.route(id,station_start,station_finish,time_start,time_in_way) values
        (null,'1','2','07:00:00','03:00:00'),
        (null,'2','1','10:00:00','03:00:00'),
        (null,'2','3','18:00:00','03:00:00');

insert into db_kp6java.trip(id,route_id,date_start,date_finish) values
        (null ,1,'2014-05-25','2014-05-25'),
        (null ,2,'2014-05-21','2014-05-21'),
        (null ,3,'2014-05-11','2014-05-11');

insert into db_kp6java.classtrip(id,name,description) values
        (null ,'first','seat'),
        (null ,'second','kupe');

insert into db_kp6java.price(id,route_id,classTrip_id,all_count,count_in_presence,summa) values
        (null ,1,1,100,100,2000),
        (null ,1,2,100,100,4000),
        (null ,2,1,100,100,2000),
        (null ,2,2,100,100,4000),
        (null ,3,1,100,100,2000),
        (null ,3,2,100,100,4000);

